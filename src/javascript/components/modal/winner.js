import { showModal } from './modal'
import { createFighterImage } from '../fighterPreview'

export function showWinnerModal(fighter) {
  // call showModal function 
  const winner = createFighterImage(fighter);
  winner.classList.add('modal-image');

  const elementModal = {
     title: `Winner - ${fighter.name}`,
     bodyElement: winner,
     onClose: () => {
       location.reload();
     }
  };

  showModal(elementModal);
}
