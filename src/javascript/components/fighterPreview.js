import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

    const { source, name, health, attack, defense } = fighter;
    const attributes = {
      src: source,
      title: name,
      alt: name
    };
  
    fighterElement.innerHTML += `<div class="fighter-preview___text">
                                <h2>${name}:</h2>
                                <p>Health: ${health}%; Attack: ${attack}; Defense: ${defense}</p>
                                </div>`
    const imgPreview = createElement({
      tagName: 'img',
      attributes,
    })
    
    fighterElement.append(imgPreview);
    return fighterElement;
}



export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
