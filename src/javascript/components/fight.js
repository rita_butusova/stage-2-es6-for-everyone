import { controls } from '../../constants/controls';
import { showWinnerModal } from './modal/winner'

export async function fight(firstFighter, secondFighter) {
  firstFighter.counterDamege = 0;
  secondFighter.counterDamege = 0;
  return new Promise((resolve) => {
  // resolve the promise with the winner when fight is over
  let pressed = new Set();
  document.addEventListener('keydown', function (event) {
  pressed.add(event.code);

  function delay(f, ms) {
    return function() {
      setTimeout(() => f.apply(this, arguments), ms);
    };
  }

  if (pressed.has(...controls.PlayerOneCriticalHitCombination)) {
    secondFighter.side = 'right-fighter-indicator';
    delay(getDamageCritical(firstFighter, secondFighter), 10000);
  }
  if ((pressed.has(...controls.PlayerTwoCriticalHitCombination))) {
    firstFighter.side = 'left-fighter-indicator';
    delay(getDamageCritical(secondFighter, firstFighter), 10000);
  }

  if ((pressed.has(controls.PlayerOneAttack)) 
      && (pressed.has(controls.PlayerTwoBlock))) {
        secondFighter.side = 'right-fighter-indicator';
        getDamage(firstFighter, secondFighter);
  }
  if ((pressed.has(controls.PlayerOneBlock)) 
      && (pressed.has(controls.PlayerTwoAttack))) {
        firstFighter.side = 'left-fighter-indicator';
        getDamage(secondFighter, firstFighter);
  }
  if ((pressed.has(controls.PlayerOneAttack))) {
    secondFighter.side = 'right-fighter-indicator';
    getDamage(firstFighter, secondFighter);
  }
  if ((pressed.has(controls.PlayerTwoAttack))) {
    firstFighter.side = 'left-fighter-indicator';
    getDamage(secondFighter, firstFighter);
  }


  if (firstFighter.counterDamege >= 100) {
    resolve(secondFighter);
    showWinnerModal(secondFighter);
  } else if (secondFighter.counterDamege >= 100) {
    resolve(firstFighter);
    showWinnerModal(firstFighter);
  }
  });

  document.addEventListener('keyup', function (event) {
    pressed.delete(event.code);
  });

});
}

function getDamageCritical(attacker, defender) {
  const attack = attacker.attack * 2;
  let percentAttack = attack * 100 / defender.health;
  changeHealth(percentAttack, defender)
}

export function getDamage(attacker, defender) {
  // return damage or percent damage
  let damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0) {
    damage = 0;
  }
  let percentHit = damage * 100 / defender.health;
  changeHealth(percentHit, defender);
}

export function getHitPower(fighter) {
  // return hit power
  const attack = fighter.attack;
  const criticalHitChance = Math.random() * (2 - 1) + 1;
  const powerHit = attack * criticalHitChance;
  return powerHit;
}

export function getBlockPower(fighter) {
  // return block power
  const defense = fighter.defense;
  const dodgeChance = Math.random() * (2 - 1) + 1;
  const powerBlock = defense * dodgeChance;
  return powerBlock;
}

function changeHealth(percentHit, defender) {
  const startSizeBar = document.querySelector('.arena___fighter-indicator').offsetWidth;
  // indicator health
  let health_bar;
  if (defender.side == 'left-fighter-indicator') {
    health_bar = document.getElementById('left-fighter-indicator')
  } else {
    health_bar = document.getElementById('right-fighter-indicator')
  }
  // percent health for indicator
  let sizeBar = health_bar.offsetWidth;
  // increase counter
  defender.counterDamege = defender.counterDamege + percentHit;

  //change health player (for check)
  defender.health -= defender.health * percentHit/100;
  
  // lower bar
  sizeBar = sizeBar - (startSizeBar * (percentHit/100));
  health_bar.style.width = sizeBar + 'px';
  //delete choice side from object
  delete defender.side;
}